import csv from 'csvtojson';
import mongoose from 'mongoose';
import { Orders, Users } from './src/db';
import config from './config';

mongoose.connect(config.MONGO_URI, {useNewUrlParser: true});

const ordersFile = './testdata/orders.csv';
const usersFile = './testdata/users.csv';

const importToDatabase = async (path, model) => {
    await csv()
    .fromFile(path)
    .then(async (json) => {
        await json.forEach(async (obj, i) => {
            const newRecord = await new model({ _id: new mongoose.Types.ObjectId() ,...obj});
    
            await newRecord.save();
        });
    });
}

importToDatabase(ordersFile, Orders);
importToDatabase(usersFile, Users);