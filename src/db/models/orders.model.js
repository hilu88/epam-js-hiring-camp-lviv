import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const ordersSchema = new Schema({
    _id: Schema.Types.ObjectId, 
    userId: {
        type: Number,
        required: true
    },
    product: {
        type: String,
        required: true
    },
    status: {
        type: String,
        required: true,
    },
    quantity: {
        type: Number,
        required: true,
        default: 0
    }
});

export const Orders = mongoose.model('orders', ordersSchema);