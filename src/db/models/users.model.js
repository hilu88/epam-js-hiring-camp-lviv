import mongoose from 'mongoose';
const AutoIncrement = require('mongoose-sequence')(mongoose);

const Schema = mongoose.Schema;

const usersSchema = new Schema({
    _id: Schema.Types.ObjectId, 
    userId: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    status: {
        type: String,
        required: true,
    }
});

usersSchema.plugin(AutoIncrement, {
    inc_field: 'userId' 
});

export const Users = mongoose.model('users', usersSchema);