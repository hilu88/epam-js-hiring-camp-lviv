import { Users } from './users.model';
import { Orders } from './orders.model';

export {
    Users,
    Orders
}