import express from 'express';
import mongoose from 'mongoose';
import config from '../config';
import routes from './routes';

const app = express();

mongoose.connect(config.MONGO_URI, {useNewUrlParser: true})
.then(() => console.log(`[${new Date()}] Database connected, status: Good!`))
.catch((err) => console.log(`[${new Date()}] Database error, status: Bad!`, err));

routes(app);

app.get('/', function (req, res) {
  res.status(200).send({
    last_updated: new Date(),
    status: 'Good'
  });
});

app.listen(config.PORT,  () =>  console.log(`Example app listening on port ${config.PORT}!`));