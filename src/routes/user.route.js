import express from 'express';
import { Users, Orders } from '../db';

const router = express.Router();

router.get('/orders', async (req, res) => {
    const orders = await Orders.find(
        { status: 'purchased' }, 
        (error, response) => console.log(response)
    ).limit(3);

    console.log(orders);
    res.status(200).send(orders)
})

export default router;