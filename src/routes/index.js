import UserRoute from './user.route';

export default (app) => {
    app.use('/user', UserRoute)
};